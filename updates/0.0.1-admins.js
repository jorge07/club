/**
 * This script automatically creates a default Admin user when an
 * empty database is used for the first time. You can use this
 * technique to insert data into any List you have defined.
 * 
 * Alternatively, you can export a custom function for the update:
 * module.exports = function(done) { ... }
 */

exports.create = {
	User: [
		{ 'name.first': 'Leonardo', 'name.last': 'Arco', email: 'clubleoskimdo@gmail.com', password: 'club2015', isAdmin: true },
        { 'name.first': 'Jorge',    'name.last': 'Arco', email: 'jorge.arcoma@gmail.com',  password: 'aA123123', isAdmin: true }
	]
};