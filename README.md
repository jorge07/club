# Club Leos

The **Club Leos** website evolution

# Require MongoDB

# Requirements as Global

    - Bower 
        - npm install - g bower
    - Grunt
        - npm install - g grunt

# To install:

    - Download the repository
    
    - cd /ClubLeos
    
    - Download dependencies: 
        - npm install 

    - Download assets: 
        - bower install

    - Init development environment: 
        - grunt devel

    - Start application: 
        - grunt build && node keystone
    
    - Go 
        -http://localhost:3000