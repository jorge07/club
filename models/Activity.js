var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Activity Model
 * ==========
 */

var Activity = new keystone.List('Activity', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Activity.add({
	title: { type: String, required: true, label: 'Título' },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true , label: 'Estado'},
	author: { type: Types.Relationship, ref: 'User', index: true },
	publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } },
	image: { type: Types.CloudinaryImage, select: true },
	content: {
		brief: { type: Types.Html, wysiwyg: true, height: 150, label: 'Introducción' },
		extended: { type: Types.Html, wysiwyg: true, height: 400, label: 'Texto' }
	}
});

Activity.schema.virtual('content.full').get(function() {
	return this.content.extended || this.content.brief;
});

Activity.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%';
Activity.register();
