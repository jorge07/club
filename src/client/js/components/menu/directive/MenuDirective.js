/**
 * Created by jorge on 20/07/15.
 */
(function(angular){

    /**
     * Simple directive to bind menu controller
     *
     * Just because I like directives
     *
     * @returns {{restrict: string, controller: string}}
     * @constructor
     */
    function MenuDirective(){
        return {
            restrict: 'A',
            controller: 'MenuController'
        };
    }

    angular
        .module('leos')
        .directive('menu', [
            MenuDirective
        ]);
})(angular);