/**
 * Created by jorge on 20/07/15.
 */
(function(angular){

    /**
     * AsideService
     *
     * @constructor
     */
    function AsideService($aside)
    {
        var modalConfig = {
            templateUrl: '/templates/menu/menu.html',
            placement: 'left',
            backdrop: true,
            size: 'sm'
            },
        modal   = null,
        onClose = function(){};

        /**
         * Action on close
         *
         * @param callback
         * @returns {AsideService}
         */
        this.onClose = function (callback) {

            onClose = callback;

            return this;
        };

        /**
         * Open aside
         */
        this.open = function(){
            modal = $aside.open(modalConfig);
            modal.result.then(function () {},
            onClose);
        };

        /**
         * Close menu modal
         */
        this.closeMenu = function() {
            modal.close();
        };
    }



    /**
     * Definition
     */
    angular
        .module('leos')
        .service('LeosAside', [
            '$aside',
            AsideService
        ]);

})(angular);