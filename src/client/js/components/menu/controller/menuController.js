/**
 * Created by jorge on 20/07/15.
 */
(function(angular){

    /**
     * Menu Controller
     *
     * @param $window
     * @param $scope
     * @param LeosAside
     * @param SecurityAccess
     * @param $timeout
     * @param $mdSidenav
     * @constructor
     */
    function MenuController($window, $scope, LeosAside, SecurityAccess)
    {

        /**
         * Modal state
         *
         * @type {boolean}
         */
        var modalState = false;

        /**
         * Event on modal close
         */
        LeosAside.onClose(function () {
            $scope.$broadcast('modal-closing');
        });

        /**
         * Toggle menu
         */
        $scope.toggleMenu = function () {

            if (modalState) {

                LeosAside.closeMenu();

            } else {

                LeosAside.open();

            }

            modalState = !modalState;

        };

        /**
         * Modal close event
         */
        $scope.$on('modal-closing', function () {
            modalState = false;
        });

        /**
         * Get Modal state
         *
         * @returns {boolean}
         */
        $scope.getModalState = function () {
            return modalState;
        };

        /**
         * Open aside
         */
        $scope.open = function () {
            if (!modalState) {
                LeosAside.open();
            }

            modalState = true;
        };

        /**
         * Close aside
         */
        $scope.close = function () {
            if (modalState) {
                LeosAside.closeMenu();
            }

            modalState = false;
        };

        /**
         * Redirect to login
         */
        $scope.loginUser = function () {

            SecurityAccess.login();
        };

        /**
         * Logout redirection
         */
        $scope.logoutUser = function () {

            SecurityAccess.logout();
        };

        /**
         * Open call
         */
        $scope.callMeBaby = function () {

            $window.open('tel:661751612', '_system');
        };

        /**
         * Send email
         */
        $scope.sendMeAnEmailBaby = function () {

            $window.open('mailto:clubleosimdo@gmail.com?Subject=Hola%20Leonardo!', '_system');
        };

        /**
         * Open facebook
         */
        $scope.followMe = function () {

            $window.open('https://es-es.facebook.com/LeosKimDo', '_blank');
        };
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .controller('MenuController', [
            '$window',
            '$scope',
            'LeosAside',
            'SecurityAccess',
            MenuController
        ]);
})(angular);