/**
 * Created by jorge on 28/07/15.
 */
(function(angular){

    /**
     * Map directive generator
     *
     * @doc
     * Require a {destination} object inside the $scope and an ID attr in the html tag.
     *
     * @returns {{restrict: string, controller: *[]}}
     * @constructor
     */
    function GoogleMaps(){


        return {
            restrict: 'E',
            link: function(scope, $element, $attrs){

                var node = document.getElementById($attrs.id);


                /**
                 * StartMap function
                 */
                function startMap(){


                    /**
                     * Validate node has ID attr
                     */
                    if (node === undefined) {
                        console.error('Map Directive must have ID attribute');
                        return null;
                    }

                    /**
                     * Generate LatLng
                     *
                     * @type {google.maps.LatLng}
                     */
                    var latLon = new google.maps.LatLng($attrs.lat,$attrs.lng);

                    /**
                     * Config Map
                     *
                     * @type {google.maps.Map}
                     */
                    var map = new google.maps.Map(
                        node,
                        {
                            center: latLon,
                            zoom: 18,
                            draggable: false,
                            scrollwheel: false,
                            mapTypeId:google.maps.MapTypeId.HYBRID
                        });

                    /**
                     * Add Marker to the map
                     *
                     * @type {google.maps.Marker}
                     */
                    var marker = new google.maps.Marker({
                        position: latLon,
                        map: map,
                        title: $attrs.name,
                        icon: 'http://isvr.net/maps/marker_img.png'
                    });

                    /**
                     * Create infoWindow
                     *
                     * Html inside to make it faster
                     *
                     * @type {google.maps.InfoWindow}
                     */
                    //var infowindow = new google.maps.InfoWindow({
                    //    content: '<p>We are on <h2>' + $scope.destination.name + '</h2>' +
                    //    '<br>' +
                    //    'Country: <strong>' + $scope.destination.country + '</strong></p>'
                    //});

                    /**
                     * Add Event on click
                     */
                    //google.maps.event.addListener(marker, 'click', function() {
                    //    infowindow.open(map,marker);
                    //});

                }


                startMap();
            }
        };
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .directive('leosMap', [
            GoogleMaps
        ]);
})(angular);
