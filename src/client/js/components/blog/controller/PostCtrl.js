/**
 * Created by jorge on 3/08/15.
 */
(function(angular){

    /**
     * Post Controller
     *
     * @param $scope
     * @param post
     * @param $sce
     * @constructor
     */
    function PostController($scope, post, $sce)
    {
        $scope.post = post.data.post;
        $scope.post.description = $sce.trustAsHtml($scope.post.description);
    }

    angular
        .module('leos')
        .controller('PostCtrl', [
            '$scope',
            'post',
            '$sce',
            PostController
        ]);
})(angular);