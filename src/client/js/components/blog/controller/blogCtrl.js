/**
 * Created by jorge on 27/07/15.
 */
(function(angular){

    /**
     * Blog controller
     *
     * Bind the Service with the view, that's all
     *
     * @param $scope
     * @param Blog
     * @param $sce
     * @constructor
     */
    function BlogController($scope, Blog, $sce)
    {
        angular.extend($scope, {
            getAll :         Blog.get,
            createdPost :    Blog.postCreated,
            blogFilter :     Blog.filter,

            /**
             * Display a Demo of the content in real time
             *
             * @param post
             * @returns {*}
             */
            descriptionDemo : function(post){
                if (post !== undefined) {
                    return $sce.trustAsHtml(post.description);
                }
            }
        });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .controller('BlogCtrl', [
            '$scope',
            'Blog',
            '$sce',
            BlogController
        ]);

})(angular);