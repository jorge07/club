/**
 * Created by jorge on 27/07/15.
 */
(function(angular){

    /**
     * Blog Service
     *
     * @param $http
     * @constructor
     */
    function Blog($http) {

        var self = this;

        /**
         * Private blogs var
         *
         * It's private to not allow 3º parties change this var
         * without Blog Service management
         *
         * @type {Array}
         */
        var blogs = [];

        /**
         * On created post response
         *
         * @type {null}
         */
        var postCreated = null;

        /**
         * Get Blog from API
         */
        function getBlog() {
            var call = $http.get('/api/posts');

            call.then(function (response) {
                blogs = response.data.posts;
            });

            return call;
        }

        /**
         * Get Blog
         *
         * @returns {Array}
         */
        function get() {
            return blogs;
        }

        /**
         * Find one post by name
         *
         * @param name
         */
        function findOneByName(name) {
            return $http.get('/api/posts/' + name);
        }

        /**
         * AutoLoad
         */
        getBlog();

        /**
         * Public Methods and attrs
         */
        this.get = get;
        this.findOneByName = findOneByName;
        this.reload = getBlog;
        this.postCreated = function(){

            return postCreated;
        };

        /**
         * Destination list filter
         *
         * @type {null|string|object}
         */
        this.filter = {
            query: ''
        };

    }

    angular
        .module('leos')
        .service(
        'Blog',
        [
            '$http',
            Blog
        ]);

})(angular);