/**
 * Created by jorge on 21/08/15.
 */
(function(angular){

    /**
     *
     * @param $scope
     * @param $modalInstance
     * @constructor
     */
    function ModalInstance($scope, $modalInstance) {
        
        $scope.close = function () {
            $modalInstance.dismiss('close');
        };
    }

    /**
     * Definition and directive
     */
    angular
        .module('leos')
        .controller('ModalInstanceCtrl', [
            '$scope',
            '$modalInstance',
            ModalInstance
        ]);
})(angular);