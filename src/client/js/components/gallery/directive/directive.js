/**
 * Created by jorge on 22/10/15.
 */
(function(angular){

    /**
     * Gallery Ctrl
     *
     * @param $scope
     * @param GalleryService
     * @constructor
     */
    function GalleryCtrl($scope, GalleryService)
    {
        angular.extend($scope, {
            list : GalleryService.list,
            get :  GalleryService.get
        });
    }

    angular
        .module('leos')
        .controller('GalleryCtrl', ['$scope', 'GalleryService', GalleryCtrl])
        .directive('gallery', [function(){
            return {
                restrict: 'EA',
                controller: 'GalleryCtrl'
            };
        }])
    ;

})(angular);