/**
 * Created by jorge on 22/10/15.
 */
(function(angular){

    /**
     * Gallery service
     *
     * @param $http
     * @returns {{list: Function, get: Function}}
     * @constructor
     */
    function Gallery($http)
    {

        /**
         * Private galleries
         *
         * @type {Array}
         */
        var galleries = [];

        function formatImages (k) {
            var gallery = galleries[k];

            if (gallery.formated) {
                return gallery;
            }

            var images = [];
            for (var img in gallery.images) {
                var image = gallery.images[img];
                images.push(
                    {thumb: image.secure_url, img: image.secure_url, description: image.public_id}
                );

            }

            gallery.images = images;
            gallery.formated = true;

            return gallery;
        }

        function load ()
        {
            if (galleries.length === 0) {
                $http
                    .get('/api/galleries')
                    .success(function (res) {
                        galleries = res.galleries;
                    })
                    .catch(function (err) {
                        console.error('Gallery', err);
                    });
            }
        }

        /**
         * Auto load
         */
        load();

        return {
            /**
             * Get list
             *
             * @returns {Array}
             */
            list: function () {
                return galleries;
            },
            /**
             * Get gallery
             *
             * @param id
             * @returns {*}
             */
            get: function (id) {
                for (var k in galleries) {
                    var gallery = galleries[k];
                    if (gallery._id === id) {
                        return formatImages(k);
                    }
                }
                return null;
            }

        };
    }


    /**
     * Definition
     */
    angular
        .module('leos')
        .service('GalleryService', [
            '$http',
            Gallery
        ])
    ;
})(angular);