/**
 * Created by jorge on 10/09/15.
 */
(function(angular) {

    /**
     * action
     *
     * @param scope
     * @param elem
     */
    function link(scope, elem)
    {
        elem.on('touchstart', function(e) {
            elem[0].focus();
            e.preventDefault();
            e.stopPropagation();
        });
    }


    /**
     * Definition
     */
    angular
        .module('leos')
        .directive('input', [
            function() {
                return {
                    restrict: 'E',
                    link: link
                };
            }
        ])
        .directive('textarea', [
            function() {
                return {
                    restrict: 'E',
                    link: link
                };
            }
        ]);

})(angular);