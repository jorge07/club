/**
 * Created by jorge on 13/07/15.
 */
(function(angular){

    /**
     * Add here the logic for the slider
     *
     * @constructor
     */
    function SliderService(){

        var slides      = [],
            interval    = 4500;

        /**
         * Add slide
         *
         * @param slide
         * @returns {SliderService.add}
         */
        function add(slide)
        {
            slides.push(slide);

            return this;
        }

        /**
         * Get Slides
         *
         * @returns {Array}
         */
        function get()
        {
            return slides;
        }

        /**
         * Default
         */
        add({
            text :  'Club Leo\'s Kim Do',
            style:  'background-color: #000; ' +
                    'font-size: 40px; ' +
                    'color: black; ' +
                    'font-weight: 900; ' +
                    'text-align: right;' +
                    'background-image: url(\'/images/Taekwondo-13.jpg\');' +
                    'background-color: rgb(0, 0, 0);' +
                    'background-size: cover;' +
                    'background-position: 90% 50%;' +
                    'background-repeat: no-repeat;' +
                    'text-align: right',
            textStyle: 'font-size: 50px;bottom: 80px;'
        });

        /**
         * Default
         */
        add({
            text :  'Algo más que deporte',
            style:  'background-color: #000; ' +
            'font-color: #000; ' +
            'color: #000; ' +
            'font-size: 45px; ' +
            'text-align: right;' +
            'background-image: url(\'/images/haidom.jpg\');' +
            'background-color: rgb(0, 0, 0);' +
            'background-size: cover;' +
            'background-position: 40% 50%;' +
            'background-repeat: no-repeat;' +
            'text-align: right',
            textStyle: 'top: 50px;' +
            'font-size: 50px;'

        });

        return {
            get: get,
            add: add,

            /**
             * Get slider interval
             *
             * @returns {number}
             */
            getInterval: function(){

                return interval;
            },
            setinterval: function(newInterval){
                interval = newInterval;

                return interval;
            }

        };
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .service('leosSlider', [
            SliderService
        ]);
})(angular);