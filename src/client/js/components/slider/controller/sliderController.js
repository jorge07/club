/**
 * Created by jorge on 13/07/15.
 */
(function(angular){

    /**
     * Slider Controller
     *
     * @param $scope
     * @param leosSlider
     * @constructor
     */
    function Slider($scope, leosSlider){

        $scope.myInterval = leosSlider.getInterval;
        $scope.slides = leosSlider.get;

        $scope.addSlide = function(){

            leosSlider.add({
                image: '/images/fight.jpg',
                text: 'Example'
            });
        };

    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .controller('SliderController', [
            '$scope',
            'leosSlider',
            Slider
        ]);

})(angular);