/**
 * Created by jorge on 13/08/15.
 */
(function(angular){

    /**
     * ReSizer
     *
     * @param $window
     *
     * @returns {Function}
     *
     * @constructor
     */
    function ReSizer($window) {
        return function (scope, element, attr) {

            var w = angular.element($window);
            scope.$watch(function () {
                return {
                    'h': window.innerHeight,
                    'w': window.innerWidth
                };
            }, function (newValue, oldValue) {
                scope.windowHeight = newValue.h;
                scope.windowWidth = newValue.w;

                scope.resizeWithOffset = function (offsetH) {

                    scope.$eval(attr.notifier);

                    return {
                        'height': (newValue.h - offsetH) + 'px'
                        //,'width': (newValue.w - 100) + 'px'
                    };
                };

            }, true);

            w.bind('resize', function () {
                scope.$apply();
            });
        };
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .directive('leosResize', [
            '$window',
            ReSizer
        ]);

})(angular);