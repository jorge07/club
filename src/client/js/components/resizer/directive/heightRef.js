/**
 * Created by jorge on 27/05/15.
 */
(function(angular){

    "use strict";

    /**
     * Start Parallax
     *
     * @returns {{restrict: string, link: Function}}
     * @constructor
     */
    function HeightRef($window)
    {
        return {
            restrict: 'A',
            link: function (scope, elem, attr) {

                function applyHeight()
                {
                    angular.element( document.getElementById( attr.heightRef)).css("height",
                        (angular.element( document.getElementById( attr.heightFrom))[0].offsetHeight * attr.heightRatio) +'px');
                }

                try {
                    applyHeight();

                    angular.element($window)
                        .bind('resize', applyHeight)
                        .bind('scroll', applyHeight);

                    scope.$on('destroy', applyHeight);

                } catch (err) {
                    console.log(err);
                }

            }
        };
    }

    angular
        .module('leos')
        .directive(
        'heightRef',
        [
            '$window',
            HeightRef
        ]
    );

})(angular);