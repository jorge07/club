/**
 * Created by Jorge on 08/08/2015.
 */
(function(angular){

    /**
     * Definition
     */
    angular
        .module('leos')
        .directive('resize',['$window', function ($window) {
            return function (scope, element, attr) {

                var w  = window.www = angular.element($window);

                scope.$watch(function () {
                    return {
                        'h': w[0].innerHeight,
                        'w': w[0].innerWidth
                    };
                }, function (newValue, oldValue) {
                    scope.windowHeight = newValue.h;
                    scope.windowWidth = newValue.w;

                    scope.resizeWithOffset = function (offsetH) {
                        return {
                            'height': (newValue.h - offsetH) + 'px'
                            //,'width': (newValue.w - 100) + 'px'
                        };
                    };

                }, true);

                w.bind('resize', function () {
                    scope.$apply();
                });
            };
        }]);

})(angular);