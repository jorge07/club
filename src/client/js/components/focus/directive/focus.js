/**
 * Created by jorge on 9/09/15.
 */
(function(angular){
    angular
        .module('leos')
        .directive('focus',

            function($timeout) {

                return {

                    scope : {

                        trigger : '@focus'

                    },

                    link : function(scope, element) {

                        scope.$watch('trigger', function(value) {

                            if (value === "true") {

                                $timeout(function() {

                                    element[0].focus();

                                });
                            }
                        });
                    }

                };

            }

    );
})(angular);