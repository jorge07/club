/**
 * Created by jorge on 30/07/15.
 */
(function(angular){

    /**
     * Security Service
     *
     * @param $modal
     * @constructor
     */
    function Security($modal)
    {

        /**
         * Open Login modal with a size
         *
         * @param size
         */
        this.openLogin = function(size)
        {
            $modal.open({
                templateUrl: '/templates/user/login-modal.html',
                size: size || 'sm',
                controller: 'ModalInstanceCtrl'
            });
        };

    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .service('Security', [
            '$modal',
            Security
        ]);
})(angular);