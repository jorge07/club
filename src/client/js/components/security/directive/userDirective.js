/**
 * Created by jorge on 30/07/15.
 */
(function(angular){

    /**
     * Customer Directive
     *
     * Contains all the possible Customer actions
     *
     * @example
     *      ng-click="openLogin()"
     * @returns {{restrict: string, controller: *[]}}
     * @constructor
     */
    function CustomerDirective()
    {

        /**
         * CustomerDirectiveController
         *
         * @param $scope
         * @param Security
         * @constructor
         */
        function CustomerDirectiveController($scope, Security)
        {
            $scope.openLogin = Security.openLogin;
        }

        return {
            restrict: 'EA',
            controller: ['$scope', 'Security', CustomerDirectiveController]
        };

    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .directive('customer', [
            CustomerDirective
        ]);

})(angular);