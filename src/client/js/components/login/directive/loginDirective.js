/**
 * Created by jorge on 2/10/15.
 */
/**
 * Created by jorge on 9/09/15.
 */
(function(angular){

    /**
     * Login Directive
     *
     * @returns {{restrict: string, controller: *[]}}
     * @constructor
     */
    function LoginDirective()
    {
        return {
            restrict: 'AE',
            templateUrl: '/templates/user/login-form.html',
            controller: ['$scope', 'SecurityAccess', function($scope, SecurityAccess) {

                $scope.loginStatus  = null;
                $scope.modalOpen    = true;

                $scope.login = function(data) {

                    SecurityAccess
                        .loginCheck(data)
                        .then(function(res){
                            $scope.loginStatus = res.data.status;
                            $scope.$emit('login:success');
                        });
                };
            }]
        };
    }

    /**
     * Definition and directive
     */
    angular
        .module('leos')
        .directive('login', ['SecurityAccess',
            LoginDirective
        ]);

})(angular);