/**
 * Created by jorge on 9/09/15.
 */
(function(angular){

    /**
     * Access class
     *
     * @param $http
     * @param $rootScope
     * @param $window
     * @constructor
     */
    function Security($http, $rootScope, $window)
    {
        var user = null;

        /**
         * User
         *
         * @type {null}
         */
        $rootScope.getUser = function(){
            return user;
        };

        /**
         *
         */
        function getUserFromBack(){

            $http.get('/api/user').then(
                function success(res) {

                    if (res.data.user.name) {
                        user = res.data.user;
                    }

                },
                function error(res) {

                    console.log('Login Error', res);

                }
            );

        }

        /**
         * AutoSearch user
         */
        getUserFromBack();

        /**
         * Login
         *
         * @returns {*}
         */
        this.login = function () {
            $window.location.href = "/keystone/signin";
        };


        /**
         * Logout
         *
         * @returns {*}
         */
        this.logout = function () {
            $window.location.href = "/keystone/signout";
        };


    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .service('SecurityAccess', [
            '$http',
            '$rootScope',
            '$window',
            Security
        ]);
})(angular);