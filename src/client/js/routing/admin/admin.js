/**
 * Created by jorge on 10/07/15.
 */
/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     * Main Routing Class
     *
     * @param $stateProvider
     * @param $locationProvider
     * @constructor
     */
    function Routing($stateProvider, $locationProvider)
    {
        /**
         * Enable Html mode
         */
        $locationProvider
            .html5Mode(true);

        /**
         * Main state all states which needs header must extend this one
         */
        $stateProvider
            .state('admin', {
                url: '/admin',
                views: {
                    "header": { templateUrl: "/templates/header/header.html" }
                }
            })

            /**
             * Admin homepage
             */
            .state('dashboard', {
                parent: 'admin',
                views: {
                    "content@": { templateUrl: "/templates/blog/create.html" }
                }
            });

    }

    /**
     * Definition
     */
    angular
        .module('leos').config([
            '$stateProvider',
            '$locationProvider',
            Routing]);

})(angular);