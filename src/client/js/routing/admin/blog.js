/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     *
     *
     * @param $stateProvider
     * @constructor
     */
    function Routing($stateProvider)
    {
        $stateProvider
            .state('blog-create', {
                url: "/create",
                parent: 'admin',
                views: {
                    "content@": {
                        templateUrl: "/templates/blog/create.html",
                        controller: "BlogCtrl"
                    }
                }
            });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .config([
            '$stateProvider',
            Routing
        ]);

})(angular);