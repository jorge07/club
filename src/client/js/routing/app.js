/**
 * Created by jorge on 10/07/15.
 */
/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     * Main Routing Class
     *
     * @param $stateProvider
     * @param $locationProvider
     * @constructor
     */
    function Routing($stateProvider, $locationProvider, $urlRouterProvider)
    {

        /**
         *
         */
        $urlRouterProvider.otherwise('/');

        /**
         * Enable Html mode
         */
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        /**
         * Main state all states which needs header must extend this one
         */
        $stateProvider
            .state('app', {
                views: {
                    "header": { templateUrl: "/templates/header/header.html" },
                    "footer@": { templateUrl: "/templates/footer/footer.html" }

                }
            })

            /**
             * Homepage
             */
            .state('home', {
                url: '/',
                parent: 'app',
                views: {
                    "main@": { templateUrl: "/templates/slider/slider.html" },
                    "content@": { templateUrl: "/templates/content/home-content.html" }

                }
            });

    }

    /**
     * Definition
     */
    angular
        .module('leos').config([
            '$stateProvider',
            '$locationProvider',
            '$urlRouterProvider',
            Routing
        ]);

})(angular);