/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     * Login
     *
     * @param $stateProvider
     * @constructor
     */
    function Routing($stateProvider)
    {
        $stateProvider
            .state('login', {
                url: "/login",
                parent: 'app',
                views: {
                    "content@": {
                        templateUrl: "/templates/user/login.html"
                    }
                }
            });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .config([
            '$stateProvider',
            Routing
        ]);

})(angular);