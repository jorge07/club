/**
 * Created by jorge on 13/07/15.
 */
(function(angular){

    /**
     * Schedule
     *
     * @param $stateProvider
     * @constructor
     */
    function Routing($stateProvider)
    {
        $stateProvider
            .state('schedule', {
                url: "/horarios",
                parent:'app',
                views: {
                    "content@": { templateUrl: "/templates/about.html" }
                }
            });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .config(['$stateProvider', Routing]);

})(angular);