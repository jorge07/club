/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     *
     *
     * @param $stateProvider
     * @constructor
     */
    function Routing($stateProvider)
    {
        $stateProvider
            .state('contact', {
                url: "/contacto",
                parent: 'app',
                views: {
                    "content@": { templateUrl: "/templates/contact/locations.html" }
                }
            });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .config([
            '$stateProvider',
            Routing
        ]);

})(angular);