/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     *
     *
     * @param $stateProvider
     * @constructor
     */
    function Routing($stateProvider)
    {
        $stateProvider
            .state('about', {
                url: "/about",
                parent: 'app',
                views: {
                    "content@": { templateUrl: "/templates/about.html" }
                }
            });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .config([
            '$stateProvider',
            Routing
        ]);

})(angular);