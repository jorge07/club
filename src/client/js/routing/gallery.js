/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     *
     *
     * @param $stateProvider
     * @constructor
     */
    function Routing($stateProvider)
    {
        $stateProvider
            .state('galleries', {
                url: "/galeria",
                parent: 'app',
                views: {
                    "content@": {
                        templateUrl: "/templates/gallery/gallery-list.html"
                    }
                }
            })

            // Display Post
            .state('gallery', {
                url: "/galeria/:id",
                parent: 'app',
                views: {
                    "content@": {
                        resolve:{
                            gallery: [ '$stateParams', function($stateParams){
                                return $stateParams.id;
                            }]
                        },
                        templateUrl: "/templates/gallery/gallery-show.html",
                        controller: ['$scope', 'gallery', function($scope, gallery){
                            $scope.galleryId = gallery;
                        }]
                    }
                }
            });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .config([
            '$stateProvider',
            Routing
        ]);

})(angular);