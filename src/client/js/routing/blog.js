/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     *
     *
     * @param $stateProvider
     * @constructor
     */
    function Routing($stateProvider)
    {
        $stateProvider
            .state('blog', {
                url: "/blog/list",
                parent: 'app',
                views: {
                    "content@": {
                        templateUrl: "/templates/blog/blog-list.html",
                        controller: "BlogCtrl"
                    }
                }
            })

            // Display Post
            .state('post', {
                url: "/blog/:postName/show",
                parent: 'app',
                views: {
                    "content@": {
                        resolve:{
                            post: [ '$stateParams', 'Blog', '$sce', function($stateParams, Blog, $sce){
                                return Blog.findOneByName($stateParams.postName);
                            }]
                        },
                        templateUrl: "/templates/blog/show.html",
                        controller: 'PostCtrl'
                    }
                }
            });
    }

    /**
     * Definition
     */
    angular
        .module('leos')
        .config([
            '$stateProvider',
            Routing
        ]);

})(angular);