/**
 * Created by jorge on 10/07/15.
 */
(function(angular){

    /**
     * Definition
     */
    angular
        .module('leos', [
            'ui.router',
            'ui.bootstrap',
            'ngAnimate',
            'angular-loading-bar',
            'ng-fastclick',
            'ngTouch',
            'ngAside',
            'ngSanitize',
            'jkuri.gallery',
            'ngMaterial'
        ])
        .run(['$rootScope', '$window', '$location', function ($rootScope, $window, $location) {
            $rootScope.$on('$stateChangeSuccess', function() {
                document.body.scrollTop = document.documentElement.scrollTop = 0;

                if (!$window.ga) {
                    return;
                }

                $window.ga('send', 'pageview', { page: $location.path() });
            });
        }])
        // PRELOADER
        .run(['$rootScope', function($rootScope){

            var loaderContainer = angular.element(document.getElementsByTagName('header'));
            var content = angular.element(document.getElementById('content'));


            setTimeout(function(){
                loaderContainer.removeClass('enter');
            }, 250);

            $rootScope.$on('$stateChangeSuccess',function(){

                $rootScope.loaded = true;

                /**
                 * Init app
                 */
                setTimeout(function () {
                    loaderContainer.removeClass('loading');
                    content.removeClass('hidden');
                }, 1000);

            });

        }])
        .config(['$mdThemingProvider', function($mdThemingProvider) {
            $mdThemingProvider.theme('default')
                .primaryPalette('blue')
                .accentPalette('red');
        }]);


})(angular);