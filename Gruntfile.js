'use strict';

var config = {
    port: 3000
};

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Project configuration.
    grunt.initConfig(
        require('load-grunt-configs')(
            grunt,
            {
                config: {
                    src: './grunt/*.js'
                },
                sassMain: 'src/client/scss/main.scss',
                pkg: grunt.file.readJSON('package.json'),
                nodemon: {
                    serve: {
                        script: 'keystone.js',
                        options: {
                            ignore: ['node_modules/**']
                        }
                    }
                },
                app: 'leos',
                grunt: {
                    files: [
                        'Gruntfile.js',
                        'config/*.js'
                    ]
                },
                server: {
                    files: [
                        'src/app/**/*.js',
                        'src/app/*.js',
                        'app.js',
                        'routes/*.js',
                        'routes/**/*.js'
                    ]
                },
                js: {
                    files: [
                        'src/client/js/**/*.js',
                        'src/client/js/*.js'
                    ]
                },

                css: {
                    files: [
                        'src/client/scss/**/*.scss',
                        'src/client/scss/*.scss'
                    ]
                },

                jade: {
                    files: [
                        'src/client/templates/*.jade',
                        'src/client/templates/**/*.jade'
                    ]
                },

                images: {
                    files: [
                        'src/client/images/*.(jpg|png|gif)'
                    ]
                },

                bower: {
                    js: {
                        files: [
                            'src/client/vendor/jquery/dist/jquery.min.js',
                            'src/client/vendor/angular/angular.min.js',
                            'src/client/vendor/angular-material/angular-material.js',
                            'src/client/vendor/angular-aria/angular-aria.js',
                            'src/client/vendor/angular-touch/angular-touch.js',
                            'src/client/vendor/angular-ui-router/release/angular-ui-router.min.js',
                            'src/client/vendor/angular-aside/dist/js/angular-aside.min.js',
                            'src/client/vendor/angular-bootstrap/ui-bootstrap.min.js',
                            'src/client/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
                            'src/client/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
                            'src/client/vendor/angular-animate/angular-animate.js',
                            'src/client/vendor/angular-touch/angular-touch.min.js',
                            'src/client/vendor/ng-fastclick/dist/index.min.js',
                            'src/client/vendor/angular-loading-bar/build/loading-bar.min.js',
                            'src/client/vendor/angular-sanitize/angular-sanitize.min.js',
                            'src/client/vendor/ngGallery/src/js/ngGallery.js'


                        ]
                    },
                    css: {
                        files: [
                            'src/client/vendor/angular-aside/dist/css/angular-aside.css',
                            'src/client/vendor/angular-loading-bar/build/loading-bar.min.css',
                            'src/client/vendor/ngGallery/src/css/ngGallery.css',
                            'src/client/vendor/bootstrap/dist/css/bootstrap.min.css'
                        ]
                    }

                }

            }
        )
    );


    /**
     * Build project
     */
    grunt.registerTask('build', [
        'clean:build',
        // CSS Vendors
        'uglify:jsVendors',
        'cssmin:cssVendors',
        // Css Leos
        'sass:src',
        'cssmin:src',
        // Fonts
        'copy',
        // Images
        'imagemin',
        // Javascript
        'jshint:jsClient',
        'uglify:js',
        'clean:js',
        'clean:css',
        'injector:dist',
        // Templates
        'jade'
    ]);

    /**
     * Release project
     */
    grunt.registerTask('release', 'Building release', function () {
        grunt.log.writeln('Building Leos release.');
        grunt.task.run([
            'build',
            'cssmin:dist',
            'concat:js',
            'clean:dist',
            'injector:dist'
        ]);
    });


    // load jshint
    grunt.registerTask('lint', [
        'jshint'
    ]);

    grunt.registerTask('dev', [
        'sass:src',
        'watch'
    ]);

    // default option to connect server
    grunt.registerTask('serve', [
        'build',
        'concurrent:dev'
    ]);

    grunt.registerTask('server', function () {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve:' + target]);
    });

};
