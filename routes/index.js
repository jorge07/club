/**
 * This file is where you define your application routes and controllers.
 * 
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 * 
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 * 
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 * 
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 * 
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
var restful = require('restful-keystone')(keystone);
var sitemap = require('sitemap'),
    sm = sitemap.createSitemap({
        hostname : 'http://www.leoskimdo.com',
        cacheTime : 1000 * 60 * 24,
        urls: [
            { url: '/about', img: 'http://leoskimdo.com/images/Taekwondo-07.jpg', changefreq: 'monthly', priority: 0.3 },
            { url: '/galeria',  changefreq: 'monthly',  priority: 0.7 },
            { url: '/horarios'},
            { url: '/contacto'},
            { url: '/blog/list',changefreq: 'monthly'}
        ]
    });

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// Views
	//app.get('/api/post', routes.views.post);
    restful.expose({
        Post : {
            filter : {
                state: "published"
            },
            populate : "author"
        },
        Gallery: true,
        Activity: {
            filter : {
                state: "published"
            }
        }
    }).start();

    // Sitemap
    app.get('/sitemap.xml', function(req, res){
        sm.toXML(function(err, xml){
            res.header('Content-Type', 'application/xml');
            res.send(xml);
        });
    });
    //Robots
    app.get('/robots.txt', function (req, res) {
        res.type('text/plain');
        res.send("User-agent: *\nDisallow: /keystone");
    });

    app.get('/', routes.views.home);
    app.get('/about', routes.views.about);
    app.get('/api/user', routes.views.user);
    app.get('/*', routes.views.index);
	app.get('/*/*', routes.views.index);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
	
};
