var keystone = require('keystone');

exports = module.exports = function (req, res) {

    var view = new keystone.View(req, res);

    res.locals.title = "Club Leo's Kim Do - Nosotros";
    res.locals.seoDescription = "Conoce al equipo del Club Leo\'s Kim Do y de como nuestra pasión se ha convertido en nuestro trabajo";

    // Render the view
    view.render('about');

};
