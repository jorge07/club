var keystone = require('keystone');
var async = require('async');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	// Init locals
	locals.section = 'blog';
	locals.filters = {
		category: req.params.category
	};
	locals.data = {
		posts: [],
		categories: []
	};

	// Load the posts
	view.on('init', function(next) {
		
		keystone.list('Activity').findOne({
				slug: req.params.slug
			})
			.where('state', 'published')
			.sort('-publishedDate')
			.populate('author categories')
            .exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});
		
	});
	
	// Render the view
	view.render('blog');
	
};
