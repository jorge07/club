var keystone = require('keystone');

exports = module.exports = function(req, res) {
    var user = {};

    if (req.user) {
        user = {
            name: req.user.name
        }
    }

    res.setHeader('Content-Type', 'application/json');
	res.json({
        user : user
    });
};
