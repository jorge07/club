// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({

	'name': 'ClubLeos',
	'brand': 'Club Leo\'s',
    'signin logo' : '/images/logoleos.png',
	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'jade',
	'auto update': true,
	'session': true,
    'session store':'connect-redis',
    'session store options': {
        'port': process.env.redisPort || 6379,
        'host': process.env.redisHost || 'localhost',
        'pass': process.env.redisPrimaryKey || process.env.redisSecondaryKey || null
    },
	'auth': true,
	'user model': 'User',
    'cookie secret': 'HN|>i)-QkFX5-.%H(Kbf?Q?g2OZorTt}ydR/0)H)3"z,#7[o<-^Q_KoZ7L2!Bo!i',
    'cloudinary config': 'cloudinary://876397654462729:6OiMqlYRNS5JFTWexBu_lP1rYWY@clubleos',
    'wysiwyg images': true,
    'wysiwyg cloudinary images' : true,
    'compress' : true

});

// Load your project's Models

keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

// Load your project's Routes

keystone.set('routes', require('./routes'));

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
	'Blog': ['posts', 'post-categories'],
    'Actividades' : 'activities',
	'Galeria': 'galleries',
	'Contacto': 'enquiries',
	'Usuarios': 'users'
});

// Start Keystone to connect to your database and initialise the web server

keystone.start();
