/**
 * Created by Jorge on 02/12/15.
 */
var path = require('path');

module.exports = {
    dist: {
        options: {
            template: 'templates/layouts/base.jade',
            ignorePath: ['public'],
            transform: function (file) {

                if (path.extname(file).slice(1) == 'js'){

                    return 'script(src="' + file + '")';

                }else if(path.extname(file).slice(1) == 'css'){

                    return 'link(href="' + file + '", rel="stylesheet")';

                }

                return null;
            }
        },
        files: {
            'templates/layouts/default.jade': [
                'public/stylesheets/*.css',
                'public/javascripts/*.js'
            ]
        }
    }
};