/**
 * Created by jorge on 13/07/15.
 */
module.exports = function (grunt) {

    return {
        main: {                                 // Target
            options: {                          // Target options
                optimizationLevel: 7,
                    svgoPlugins: [
                        { removeViewBox: false }
                    ]
            },
            files: [{
                expand: true,                       // Enable dynamic expansion
                cwd: 'src/client/images/',          // Src matches are relative to this path
                src: ['**/*.{png,jpg,gif,ico}'],    // Actual patterns to match
                dest: 'public/images/'              // Destination path prefix
            }]
        }
    };
};