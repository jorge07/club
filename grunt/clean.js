/**
 * Created by Jorge on 02/12/15.
 */
module.exports = {
    build: {
        src: [
            "public"
        ]
    },
    js: {
        src: [
            "public/*/*.js",
            "!public/*/*.min.js"
        ]
    },
    css: {
        src: [
            "public/*/*.css",
            "!public/*/*.min.css"
        ]
    },
    dist: {
        src: [
            "public/*/*.js",
            "!public/*/dist-*.js",
            "public/*/*.css",
            "!public/*/dist-*.css"
        ]
    }
};