/**
 * Created by jorge on 10/07/15.
 */
module.exports = {
    js: {
        files: {
            'public/javascripts/<%= app %>.min.js': '<%= js.files %>'
        }
    },
    jsVendors: {
        files: {
            'public/javascripts/vendor.min.js': '<%= bower.js.files %>'
        }
    }
};