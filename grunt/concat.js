/**
 * Created by Jorge on 02/12/15.
 */
module.exports = {
    js: {
        src: [
            'public/javascripts/vendor.min.js',
            'public/javascripts/<%= app %>.min.js'
        ],
        dest: 'public/javascripts/dist-leos.min.js'
    }
};