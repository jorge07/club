module.exports = {
    options: {
        sourceMap: true
    },
    // Task
    dist: { // Target
        files: {                         // Dictionary of files
            'public/stylesheets/vendor.min.css': '<%= bower.css.files %>'
        }
    },
    src: {
        files: {
            'public/stylesheets/<%= app %>.css': 'src/client/scss/main.scss'
        }
    }
};
