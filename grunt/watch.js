module.exports = {
	express: {
		files: [
			'keystone.js',
            'routes/*.js',
            'routes/**/*.js'
        ],

		tasks: ['jshint:server', 'concurrent:dev']
	},
    /**
     * JS Client
     */
    js: {
        files: '<%= js.files %>',
        tasks: ['jshint:jsClient', 'uglify:js']
    },

    /**
     * Css files
     */
    sass: {
        options: {
            nospawn: true,
            interrupt: true
        },
        files: '<%= css.files %>',
        tasks: ['sass:src', 'cssmin:src' ]

    },

    /**
     * Compile jade templates
     */
    jade: {
        files: '<%= jade.files %>',
        tasks: ['jade']
    },
    /**
     * images config
     */
    images: {
        files: '<%= images.files %>',
        task: ['imagemin']
    },
    triggerLiveReloadOnTheseFiles: {
        options: {
            livereload: 1337

        },
        files: [
            '<%= js.files %>',
            '<%= css.files %>',
            '<%= jade.files %>'
        ]
    }
};
