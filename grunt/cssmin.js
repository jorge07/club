/**
 * Created by jorge on 13/07/15.
 */
module.exports = function (grunt, options) {

    return {
        cssVendors: {
            files: {
                'public/stylesheets/vendor.min.css' : '<%= bower.css.files %>'
            }
        },
        src: {
            files: {
                'public/stylesheets/<%= app %>.min.css' : 'public/stylesheets/<%= app %>.css'
            }
        },
        dist: {
            files: {
                'public/stylesheets/dist-<%= app %>.min.css': [
                    'public/stylesheets/vendor.min.css',
                    'public/stylesheets/<%= app %>.min.css'
                ]
            }
        }
    };
};
