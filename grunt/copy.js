module.exports ={
    files: {
        cwd: 'src/client/fonts',  // set working folder / root to copy
        src: '**/*',           // copy all files and subfolders
        dest: 'public/fonts',    // destination folder
        expand: true           // required when using cwd
    }
};
