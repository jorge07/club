/**
 * Created by jorge on 10/07/15.
 */
module.exports = function (grunt, options) {

    return {
        compile: {
            options: {
                client: false,
                pretty: true
            },
            files: [ {
                cwd: "src/client/templates",
                src: [
                    "*.jade",
                    "**/*.jade"
                ],
                dest: "public/templates",
                expand: true,
                ext: ".html"
            } ]
        }
    };
};