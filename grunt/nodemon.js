module.exports = {
	debug: {
		script: 'keystone.js',
		options: {
			nodeArgs: ['--debug'],
			env: {
				ENV: 'devel',
				port: 3000
			}
		}
	}
};
