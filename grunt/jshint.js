module.exports = {
	options: {
		reporter: require('jshint-stylish'),
		force: true
	},
	all: [ 'routes/**/*.js',
				 'models/**/*.js'
	],
	server: [
		'./keystone.js'
	],
    grunt: '<%= grunt.files %>',

    jsClient: '<%= js.files %>',

    jsServer: '<%= server.files %>'
};
